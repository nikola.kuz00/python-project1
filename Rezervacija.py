# import sqlite3
#
# connection = sqlite3.connect("Restoran.db")
#
# cursor = connection.cursor()
#
# sql_comm = """
# CREATE TABLE Rezervacije (
# broj_rezervacije INTEGER PRIMARY KEY,
# datum_rezervacije DATE,
# broj_osoba INTEGER,
# broj_telefona INTEGER,
# Dodaci VARCHAR(100)
# );
# """
#
# cursor.execute(sql_comm)
# connection.close()

from tabulate import tabulate
from datetime import datetime
import sqlite3


connection = sqlite3.connect("Restoran.db")

def novarezervacija(connection):
    broj_rezervacije = int(input("Unesi broj rezervacije: "))
    datum_rezervacije = input("Unesi datum rezervacije: ")
    broj_osoba = int(input("Broj osoba: "))
    # broj_telefona = input("Unesi broj telefona: ")
    while True:
        try:
            broj_telefona = int(input("Unesi broj telefona: "))
        except ValueError:
            print("Unesi pravilan broj telefona!!!")
            continue
        else:
            if len(str(broj_telefona)) == 9 or broj_telefona != 000000000:
                break
            elif len(str(broj_telefona)) >= 9 or len(str(broj_telefona)) <= 9:
                print("Unesi validan broj telefona od 9 broja!!")
            else:
                print("Unesi pravilan broj telefona")
    Dodaci = input("Posebni zahtjevi: ")
    datum_rezervacije = datetime.strptime(datum_rezervacije, '%d/%m/%Y').date()
    value = (broj_rezervacije,datum_rezervacije,broj_osoba,broj_telefona,Dodaci)
    cursor = connection.cursor()
    upit = "INSERT INTO Rezervacije (broj_rezervacije,datum_rezervacije,broj_osoba,broj_telefona,Dodaci) VALUES (?,?,?,?,?);"
    cursor.execute(upit, value)
    connection.commit()
    print("Rezervacija je unijeta!".format(cursor.rowcount))

def rezervacije(connection):
    cursor = connection.cursor()
    upit = "SELECT * FROM Rezervacije;"
    cursor.execute(upit)
    rezultat = cursor.fetchall()
    print(tabulate(rezultat, headers=["broj_rezervacije", "datum_rezervacije", "broj_osoba", "broj_telefona", "Dodaci"]))
    if cursor.rowcount < 0:
        print("Nema podataka")                      #uvijek ospisuje iako ne treba da ispisuje osim kada
    else:                                           #nema podataka uopste!!!
        for data in rezultat:
            print(data)

def brisanjerezervacije(connection):
    cursor = connection.cursor()
    rezervacije(connection)
    broj_rezervacije = int(input("Unesi broj rezervacije: "))
    value = (broj_rezervacije,)
    upit = "DELETE FROM Rezervacije WHERE broj_rezervacije = ?;"
    cursor.execute(upit, value)
    connection.commit()
    print("{} podaci uspjesno obrisani!".format(cursor.rowcount))

def izmjenarezervacije(connection):
    cursor = connection.cursor()
    rezervacije(connection)
    broj_rezervacije = int(input("Unesi broj rezervacije: "))
    datum_rezervacije = input("Unesi datum rezervacije: ")
    broj_osoba = int(input("Broj osoba: "))
    while True:
        try:
            broj_telefona = int(input("Unesi broj telefona: "))
        except ValueError:
            print("Unesi pravilan broj telefona!!!")
            continue
        else:
            if len(str(broj_telefona)) == 9 or broj_telefona != 000000000:
                break
            elif len(str(broj_telefona)) >= 9 or len(str(broj_telefona)) <= 9:
                print("Unesi validan broj telefona od 9 broja!!")
            else:
                print("Unesi pravilan broj telefona")
    Dodaci = input("Posebni zahtjevi: ")
    datum_rezervacije = datetime.strptime(datum_rezervacije, '%d/%m/%Y').date()
    upit = "UPDATE Rezervacije SET datum_rezervacije = ?, broj_osoba = ?, broj_telefona = ?, Dodaci = ? WHERE broj_rezervacije = ?;"
    value = ( datum_rezervacije, broj_osoba, broj_telefona, Dodaci, broj_rezervacije,)
    cursor.execute(upit, value)
    connection.commit()
    print("{} podaci uspjesno promijenjeni!".format(cursor.rowcount))


def Rezervacije(connection):
    print("Rezervacije:")
    print("1. Nova rezervacija!\t 2. Prikaz rezervacija!")
    print("3. Izmjena rezervacije!\t 4. Brisanje rezervacije")

    menu = input("Izaberi opciju: ")

    if menu == "1":
        novarezervacija(connection)
    elif menu == "2":
        rezervacije(connection)
    elif menu == "3":
        izmjenarezervacije(connection)
    elif menu == "4":
       brisanjerezervacije(connection)
    elif menu == "0":
        exit()
    else:
        print("!!!!")

if __name__ == "__main__":
  while(True):
    Rezervacije(connection)