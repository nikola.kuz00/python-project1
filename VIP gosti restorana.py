# import sqlite3
#
# connection = sqlite3.connect("Restoran.db")
#
# cursor = connection.cursor()
#
# sql_comm = """
# CREATE TABLE gosti_VIP (
# ID_gosta INTEGER PRIMARY KEY,
# ime_gosta VARCHAR(20),
# prezime_gosta VARCHAR(30),
# datum_rodjenja DATE
# );
# """
#
# cursor.execute(sql_comm)
# connection.close()

from tabulate import tabulate
from datetime import datetime
import sqlite3


connection = sqlite3.connect("Restoran.db")

def unospodatakagostiju(connection):
    ID_gosta = int(input("Unesi ID gosta: "))
    while True:
        ime_gosta = input("Unesi ime gosta: ")
        if len(ime_gosta) == 0:
            print("Unesi ime!!")
        elif len(ime_gosta) > 20:
            print("Ime je preveliko!!")
        else:
            break

    while True:
        prezime_gosta = input("Prezime gosta: ")
        if len(prezime_gosta) == 0:
            print("Unesi prezime!!!")
        elif len(prezime_gosta) > 30:
            print("Prezime je predugacko, unesi ponovo!!!")
        else:
            break
    datum_rodjenja = input("Unesi datum rodjenja gosta: ")
    datum_rodjenja = datetime.strptime(datum_rodjenja, '%d/%m/%Y').date()
    value = (ID_gosta, ime_gosta, prezime_gosta, datum_rodjenja)
    cursor = connection.cursor()
    upit = "INSERT INTO gosti_VIP (ID_gosta, ime_gosta, prezime_gosta, datum_rodjenja) VALUES (?,?,?,?);"
    cursor.execute(upit, value)
    connection.commit()
    print("{} je unijet!".format(cursor.rowcount))

def prikazpodataka(connection):
    cursor = connection.cursor()
    upit = "SELECT * FROM gosti_VIP;"
    cursor.execute(upit)
    rezultat = cursor.fetchall()
    print(tabulate(rezultat, headers=["ID_gosta", "ime_gosta", "prezime_gosta", "datum_rodjenja"]))
    if cursor.rowcount < 0:
        print("Nema podataka")  # uvijek ospisuje iako ne treba da ispisuje osim kada
    else:                       # nema podataka uopste!!!
        for data in rezultat:
            print(data)

def brisanjepodataka(connection):
    cursor = connection.cursor()
    prikazpodataka(connection)
    broj_rezervacije = int(input("Unesi ID gosta: "))
    value = (broj_rezervacije,)
    upit = "DELETE FROM gosti_VIP WHERE ID_gosta = ?;"
    cursor.execute(upit, value)
    connection.commit()
    print("{} podaci uspjesno obrisani!".format(cursor.rowcount))

def izmjenapodataka(connection):
    prikazpodataka(connection)
    ID_gosta = int(input("Unesi ID gosta: "))
    while True:
        ime_gosta = input("Unesi ime gosta: ")
        if len(ime_gosta) == 0:
            print("Unesi ime!!")
        elif len(ime_gosta) > 20:
            print("Ime je preveliko!!")
        else:
            break
    while True:
        prezime_gosta = input("Prezime gosta: ")
        if len(prezime_gosta) == 0:
            print("Unesi prezime!!!")
        elif len(prezime_gosta) > 30:
            print("Prezime je predugacko, unesi ponovo!!!")
        else:
            break
    datum_rodjenja = input("Unesi datum rodjenja gosta: ")
    datum_rodjenja = datetime.strptime(datum_rodjenja, '%d/%m/%Y').date()
    value = ( ime_gosta, prezime_gosta, datum_rodjenja,ID_gosta,)
    cursor = connection.cursor()
    upit = "UPDATE gosti_VIP SET ime_gosta = ?, prezime_gosta = ?, datum_rodjenja = ? WHERE ID_gosta = ?;"
    cursor.execute(upit, value)
    connection.commit()
    print("{} podaci uspjesno promijenjeni!".format(cursor.rowcount))


def VIP_gosti(connection):
    print("VIP gosti restorana:")
    print("1. Unos podataka VIP gosta!\t 2. Izmjena podataka!")
    print("3. Prikaz liste gostiju!\t 4. Brisanje podataka VIP gostiju")

    menu = input("Izaberi opciju: ")

    if menu == "1":
        unospodatakagostiju(connection)
    elif menu == "2":
        izmjenapodataka(connection)
    elif menu == "3":
        prikazpodataka(connection)
    elif menu == "4":
       brisanjepodataka(connection)
    elif menu == "0":
        exit()
    else:
        print("!!!!")

if __name__ == "__main__":
  while(True):
    VIP_gosti(connection)
