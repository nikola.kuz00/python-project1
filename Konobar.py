# import sqlite3
#
# connection = sqlite3.connect("Restoran.db")

# cursor = connection.cursor()

# sql_command = """
# CREATE TABLE Konobar(
# ID_Konobara INTEGER PRIMARY KEY,
# Ime_konobara VARCHAR(20),
# Prezime_konobara VARCHAR(30),
# Grad VARCHAR(30),
# Datum_rodjenja DATE,
# JMBG INTEGER);
# """
#
# cursor.execute(sql_command)
# connection.close()

import sqlite3
from datetime import datetime
from tabulate import tabulate
connection = sqlite3.connect("Restoran.db")

def unos_info(ID_Konobara, Ime_konobara, Prezime_konobara, Grad, Datum_rodjenja, JMBG ):
    Datum_rodjenja = datetime.strptime(Datum_rodjenja, '%d/%m/%Y').date()
    qry = "INSERT INTO Konobar (ID_konobara, Ime_konobara, Prezime_konobara, Grad, Datum_rodjenja, JMBG) VALUES (?,?,?,?,?,?);"
    connection.execute(qry,(ID_Konobara, Ime_konobara, Prezime_konobara, Grad, Datum_rodjenja, JMBG))
    connection.commit()
    print("Dodan je novi konobar!!!")

def izmjena_info(Ime_konobara, Prezime_konobara, Grad, Datum_rodjenja, JMBG, ID_Konobara):
    # qry = "UPDATE Konobar set Ime_konobara = ?, Prezime_konobara = , Grad = ?, Datum_rodjenja = ?, JMBG = ? WHERE ID_Konobara = ?;"
    # connection.execute(qry,(Ime_konobara, Prezime_konobara, Grad, Datum_rodjenja, JMBG, ID_Konobara))
    # connection.commit()
    # print("Podaci o konobaru su izmijenjeni!!")    #PROBLEM
    res = connection.cursor()
    qry = "UPDATE Konobar set Ime_konobara = ?, Prezime_konobara = ?, Grad = ?, Datum_rodjenja = ?, JMBG = ? WHERE ID_Konobara = ?;"
    konobar = (Ime_konobara, Prezime_konobara, Grad, Datum_rodjenja, JMBG,ID_Konobara)
    res.execute(qry, konobar,)
    connection.commit()
    print("Uspjesno izvrsena izmjena")

def brisanje_info(ID_Konobara):
    # qry = "DELETE FROM Konobar WHERE ID_Konobara = ?;"
    # connection.execute(qry, (ID_Konobara))
    # connection.commit()
    # print("Podaci su obrisani!!!")    #PROBLEM
    res = connection.cursor()
    qry = "DELETE FROM Konobar WHERE ID_Konobara = ?;"
    izbrisi = (ID_Konobara,)
    res.execute(qry, izbrisi)
    connection.commit()
    print("Podaci su obrisani!!")

def selektovanje_info():
    res = connection.cursor()
    qry = "SELECT * FROM Konobar"
    res.execute(qry)
    rezultat = res.fetchall()
    print(tabulate(rezultat,headers=["ID_Konobara", "Ime_konobara", "Prezime_konobara", "Grad", "Datum_rodjenja", "JMBG"]))
    if res.rowcount < 0:
        print("Nema podataka!!")
    else:
        for podaci in rezultat:
            print(podaci)

print ("""
1. Unos novih podataka.
2. Izmjena podataka.
3. Brisanje podataka.
4. Selektovanje podataka.
""")

unos_podataka = 1
while unos_podataka == 1:
    unos = int(input("Odaberi opciju: "))
    if(unos == 1):
        print("Dodaj novog konobara")
        ID_Konobara = int(input("Unesi ID: "))
        while True:
            Ime_konobara = input("Unesi ime: ")
            if len(Ime_konobara) == 0:
                print("Unesi ime!!!")
            elif len(Ime_konobara) > 20:
                print("Unesi ponovo ime!!!")
            else:
                break

        while True:
            Prezime_konobara = input("Unesi prezime: ")
            if len(Prezime_konobara) == 0:
                print("Unesi prezime!!!")
            elif len(Prezime_konobara) > 30:
                print("Unesi ponovo prezime!!!")
            else:
                break

        while True:
            Grad = input("Unesi grad: ")
            if len(Grad) == 0:
                print("Unesi grad!!!")
            elif len(Grad) > 30:
                print("Unesi ponovo grad!!!")
            else:
                break
        Datum_rodjenja = input("Unesi datum rodjenja(format: dd/mm/yyyy): ")

        while True:
            JMBG = int(input("Unesi JMBG: "))
            if len(str(JMBG)) == 13:
                break
            elif len(str(JMBG)) > 13:
                print("Unesi ponovo JMBG!!!")
            else:
                break

        unos_info(ID_Konobara, Ime_konobara, Prezime_konobara, Grad, Datum_rodjenja, JMBG)

    if (unos == 2):
        print("Izmijeni podatke konobara")
        ID_Konobara = int(input("Unesi ID: "))
        while True:
            Ime_konobara = input("Unesi ime: ")
            if len(Ime_konobara) == 0:
                print("Unesi ime!!!")
            elif len(Ime_konobara) > 20:
                print("Unesi ponovo ime!!!")
            else:
                break

        while True:
            Prezime_konobara = input("Unesi prezime: ")
            if len(Prezime_konobara) == 0:
                print("Unesi prezime!!!")
            elif len(Prezime_konobara) > 30:
                print("Unesi ponovo prezime!!!")
            else:
                break

        while True:
            Grad = input("Unesi grad: ")
            if len(Grad) == 0:
                print("Unesi grad!!!")
            elif len(Grad) > 30:
                print("Unesi ponovo grad!!!")
            else:
                break
        Datum_rodjenja = input("Unesi datum rodjenja(format: dd/mm/yyyy): ")

        while True:
            JMBG = int(input("Unesi JMBG: "))
            if len(str(JMBG)) == 13:
                break
            elif len(str(JMBG)) > 13:
                print("Unesi ponovo JMBG!!!")
            else:
                break
        izmjena_info(Ime_konobara, Prezime_konobara, Grad, Datum_rodjenja, JMBG, ID_Konobara)

    if (unos == 3):
        selektovanje_info()
        print("Izbrisi podatke o konobaru")
        ID_Konobara = input("Unes ID da bi izbrisao podatke: ")
        brisanje_info(ID_Konobara)

    if (unos == 4):
        print("Ispisi informacije o konobarima!!!")
        selektovanje_info()
    else:
        print("Greska. Probaj ponovo")
