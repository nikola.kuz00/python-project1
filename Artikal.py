# import sqlite3
#
# connection = sqlite3.connect("Restoran.db")
#
# cursor = connection.cursor()
#
# sql_comm = """
# CREATE TABLE Artikal (
# id_artikla INTEGER PRIMARY KEY,
# naziv_arikla VARCHAR(50),
# vrsta_arikla VARCHAR(30),
# cijena_artikla DECIMAL(18,2),
# rok_trajanja DATE);
# """
#
# cursor.execute(sql_comm)
# connection.close()

from tabulate import tabulate
from datetime import datetime
import sqlite3


connection = sqlite3.connect("Restoran.db")


def dodavanje_artikla(connection):
    id_artikla = int(input("Unesi id_artikla: "))
    while True:
        naziv_arikla = input("Unesi naziv: ")
        if len(naziv_arikla) == 0:
            print("Unesi naziv!!!")
        elif len(naziv_arikla) > 50:
            print("Unesi ponovo naziv!!!")
        else:
            break                                                   #greska prilikom formiranja tabele(arikla = artikla)
                                                                     #greska prilikom formiranja tabele(arikla = artikla)
    while True:
        vrsta_arikla = input("Unesi vrstu artikla: ")
        if len(vrsta_arikla) == 0:
            print("Unesi vrstu!!!")
        elif len(vrsta_arikla) > 30:
            print("Unesi ponovo vrstu!!!")
        else:
            break
    cijena_artikla = int(input("Unesi cijenu: "))
    rok_trajanja = input("Unesi datum: ")
    rok_trajanja = datetime.strptime(rok_trajanja, '%d/%m/%Y').date()
    value = (id_artikla, naziv_arikla, vrsta_arikla, cijena_artikla, rok_trajanja)
    cursor = connection.cursor()
    sql = "INSERT INTO Artikal (id_artikla, naziv_arikla, vrsta_arikla, cijena_artikla, rok_trajanja) VALUES (?,?,?,?,?)"
    cursor.execute(sql, value)
    connection.commit()
    print("{} je unijet!".format(cursor.rowcount))

def Prikaz_podataka(connection):
    cursor = connection.cursor()
    upit = "SELECT * FROM Artikal"
    cursor.execute(upit)
    rezultat = cursor.fetchall()
    print(tabulate(rezultat, headers=["id_artikla", "naziv_arikla", "vrsta_arikla", "cijena_artikla", "rok_trajanja"]))
    if cursor.rowcount < 0:
        print("Nema podataka")
    else:
        for data in rezultat:
            print(data)

def Izmjena_podataka(connection):
    cursor = connection.cursor()
    Prikaz_podataka(connection)
    id_artikla = int(input("Unesi id_artikla: "))
    while True:
        naziv_arikla = input("Unesi naziv: ")
        if len(naziv_arikla) == 0:
            print("Unesi naziv!!!")
        elif len(naziv_arikla) > 50:
            print("Unesi ponovo naziv!!!")
        else:
            break

    while True:
        vrsta_arikla = input("Unesi vrstu artikla: ")
        if len(vrsta_arikla) == 0:
            print("Unesi vrstu!!!")
        elif len(vrsta_arikla) > 30:
            print("Unesi ponovo vrstu!!!")
        else:
            break
    cijena_artikla = int(input("Unesi cijenu: "))
    rok_trajanja = input("Unesi datum: ")
    rok_trajanja = datetime.strptime(rok_trajanja, '%d/%m/%Y').date()
    upit = "UPDATE Artikal SET naziv_arikla = ?, vrsta_arikla = ?, cijena_artikla = ?, rok_trajanja = ? WHERE id_artikla = ?;"
    value = ( naziv_arikla, vrsta_arikla, cijena_artikla, rok_trajanja,id_artikla,)
    cursor.execute(upit, value,)
    connection.commit()
    print("{} podaci uspjesno promijenjeni!".format(cursor.rowcount))

def Brisanje_podataka(connection):
    cursor = connection.cursor()
    Prikaz_podataka(connection)
    id_artikla = int(input("Unesi id_artikla: "))
    value = (id_artikla,)
    upit = "DELETE FROM Artikal WHERE id_artikla = ?;"
    cursor.execute(upit, value)
    connection.commit()
    print("{} podaci uspjesno obrisani!".format(cursor.rowcount))

def pretrazivanje(connection):             #PROBLEM 
    cursor = connection.cursor()
    vrsta_arikla = input("Unesi vrstu: ")
    upit = "SELECT * FROM Artikal WHERE vrsta_arikla = ?;"
    value = ("%{}%".format(vrsta_arikla))
    cursor.execute(upit, value,)
    rezultat = cursor.fetchall()
    if cursor.rowcount < 0:
        print("There is not any data")
    else:
        for data in rezultat:
            print(data)


def Prikazi_meni(connection):
    print("Prikazi opcije:")
    print("1. Ubacivanje podataka!")
    print("2. Prikaz podataka!")
    print("3. Izmjena podataka!")
    print("4. Brisanje podataka")
    print("5. Pretrazivanje podataka!!")

    menu = input("Izaberi opciju: ")

    if menu == "1":
        dodavanje_artikla(connection)
    elif menu == "2":
        Prikaz_podataka(connection)
    elif menu == "3":
        Izmjena_podataka(connection)
    elif menu == "4":
        Brisanje_podataka(connection)
    elif menu == "5":
        pretrazivanje(connection)
    elif menu == "0":
        exit()
    else:
        print("!!!!")

if __name__ == "__main__":
  while(True):
    Prikazi_meni(connection)